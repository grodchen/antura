using Xunit;
using System;
using System.IO;
using System.Diagnostics;
using ConsoleApp1;

namespace ConsoleApp1.Test;

public class UnitTest1
{

    [Theory]
    [InlineData("apa\napa apa\n", 3)]
    [InlineData("apa\napa\n", 2)]
    [InlineData("apa\napa\nbepa\n", 2)]
    [InlineData("x\ny\n", 0)]
    [InlineData("", 0)]
     public void Test2(string text, int expectedNumber)
    {
        assertFoundNum(text, "apa.txt", expectedNumber);
        assertFoundNum(text, "a_dir/apa.txt", expectedNumber);
    }
    
    private void assertFoundNum(string fileText, string fileName, int expectedNumber)
    {
        string dirName = Path.GetDirectoryName(fileName);
        if (!dirName.Equals("")) {Directory.CreateDirectory(dirName);}
        File.WriteAllText(fileName, fileText);
        string actual = getOutputConsoleApp(fileName);
        Assert.Equal("found " + expectedNumber, actual);
    }
    
    private string getOutputConsoleApp(string fileName)
    {
        string res = "<nothing>";
        using(Process proc = new Process{
            StartInfo = new ProcessStartInfo
            {
                FileName = "ConsoleApp1.exe",
                Arguments = fileName,
                RedirectStandardOutput = true
            }})
        {
            proc.Start();
            res = proc.StandardOutput.ReadLine();
            proc.WaitForExit();
        }
        return res;
    }
}
