﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace ConsoleApp1
{
    class Program
    {
        public static string[] args;
        Program(string[] args)
        {
            Program.args = args;
        }
        private void Run()
        {
            string fileName = args[0];
            string name = Path.GetFileNameWithoutExtension(fileName);

            int counter = WordCounter.countWordInFile(name, fileName);
            Console.WriteLine("found " + counter);
        }

        static void Main(string[] args)
        {
            Program program = new Program(args);
            program.Run();
        }
    }
}
