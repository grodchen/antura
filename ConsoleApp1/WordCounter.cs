using System;
using System.Text.RegularExpressions;

namespace ConsoleApp1
{
    class WordCounter
    {
     public static int countWordInFile(string word, string fileName)
        {
            System.IO.StreamReader fileReader = new System.IO.StreamReader(fileName);
            Regex rx = new Regex(word); 
            return ReadLines(fileReader).Sum(line => rx.Matches(line).Count());
        }
     
     private static IEnumerable<String>
            ReadLines(System.IO.StreamReader fileReader)
        {           
            string? line;
            while((line = fileReader.ReadLine()) != null)
            {
                yield return line;
            }
        }
    }
}
