#### Instructions

Run tests:

    dotnet test

Run program:

    alias run='dotnet run --project ConsoleApp1'
    run FILENAME

Or just use bash commandline, e.g. for a file named ``apa.txt``:

    grep -o "apa" apa.txt | wc -l

No test input files are provided, the test creates its own files from strings.


#### Some potential todos

* Usage string for command
* Refactor to get most tests on a level above the console app level, for a simpler test setup.
* Add error messages
* Test should handle output with more than one line.

Alternative flows and corner cases that can be adressed
* wrong number of arguments (0 or >2)
* Localisation - make sure it handles special characters.
* Filename with whitespace
* File with read protection

#### Caveats and comments

* I went for creating tests on the console app instead of unit tests on the underlying functions, as I wanted
* C# is not my main language - I don't know all the language specific conventions.
* I was not allowed to install VS on my laptop, so no auto format or context assist was available.
* The ``git log`` is meant to show workflow, not the actual commits to a "real" project.
  In real life I often work with lots of commits/branches, and interactive rebase.
  They do not show completely pure TDD, but I believe TDD should be used with a pinch of salt anyways.